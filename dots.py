"""
Dots python implementation.
"""

from pprint import pprint
import random

COLORS = ['R', 'G', 'B', 'P', 'Y']

CANVAS = [
	[None, None, None, None, None, None],
	[None, None, None, None, None, None],
	[None, None, None, None, None, None],
	[None, None, None, None, None, None],
	[None, None, None, None, None, None],
	[None, None, None, None, None, None]
]

def fill_randomly(canvas):
	"""Fill the canvas randomly with all colors."""
	for line in canvas:
		for index in range(len(line)):
			line[index] = random.choice(COLORS)

def start():
	fill_randomly(CANVAS)
	pprint(CANVAS)

if __name__ == '__main__':
	start()